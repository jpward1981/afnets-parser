FROM ruby:latest

RUN { echo "[INFO] Upgrading system packages and installing required utilities." && \
      apt-get update -y && \
      apt-get upgrade -y && \
      apt-get install -y bash coreutils && \
      gem install nicinfo; }
WORKDIR /workdir
ADD  ./afnets.sh /usr/bin/afnets.sh 
RUN { echo "[INFO] Setting up afnets.sh script." && \
      chmod +x /usr/bin/afnets.sh; }
ENTRYPOINT afnets.sh
