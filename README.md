# afnets-parser

Parses, dedupes, and sorts the current list of AF networks listed with ARIN.

## Usage

For now, you'll need to do a quick Docker build:

```
$ docker build -t afnets-parser .
```

Once built, run like so to get a list of current ARIN-registered AF enterprise networks ranges:

```
$ docker run -it --rm -v $PWD:/workdir afnets-parser
```

This will return a sorted, deduped list of subnets in CIDR notation,
and this same data will be written to a timestamped file in the current working directory.
