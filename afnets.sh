#!/bin/bash

declare -A cidr_array

while read -r key values; do
  if [[ "$key" =~ CIDRs: ]]; then
    IFS=$',';
    for cidr in ${values//[[:space:]]}; do
	cidr_array["$cidr"]="${cidr}";
    done;
    unset IFS;
  fi;
done < <(nicinfo -Vt entityhandle 7ESG 2>&1);

for cidr in "${cidr_array[@]}"; do
    echo "${cidr}";
done | sort -n | tee -a "afnets.$(date --universal --iso-8601=seconds).log"
